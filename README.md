A small Go utility to monitor service status and restarts

Runs on Linux and Darwin machines.

Grab with go get:
`go get gitlab.com/mijho/servicecheck`

Clone the repo and build the binary:
```
$ git clone https://gitlab.com/mijho/servicecheck.git
$ cd servicecheck
$ go build servicecheck.go
```
