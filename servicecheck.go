package main

import (
	"bytes"
	// "fmt"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"os/exec"
	"strings"
	"strconv"
	"syscall"
)

const defaultFailedCode = 1

type Config struct {
    Services	[]string
    Logfile		string
    Ifname 		string
    Telegram struct {
    	BotId string
    	ChatId string
    }
}

func sendMessageToTelegram(m string, ci string, bi string) {
	pa := []string{"chat_id=", ci, "&text=", m, "&parse_mode=Markdown&disable_web_page_preview=true"}
	payload := strings.Join(pa, "")
	body := strings.NewReader(payload)
	requestUrlSlice := []string{"https://api.telegram.org/bot", bi, "/sendMessage"}
	requestUrl := strings.Join(requestUrlSlice, "")
	req, err := http.NewRequest("POST", os.ExpandEnv(requestUrl), body)
	if err != nil {
		log.Printf("Message '%s' was not able to be sent\n", m)
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Printf("Message '%s' was not able to be sent\n", m)
	}
	defer resp.Body.Close()	
}

func runCommand(name string, args ...string) (stdout string, stderr string, exitCode int) {
    log.Println("run command:", name, args)
    var outbuf, errbuf bytes.Buffer
    cmd := exec.Command(name, args...)
    cmd.Stdout = &outbuf
    cmd.Stderr = &errbuf

    err := cmd.Run()
    stdout = outbuf.String()
    stderr = errbuf.String()

    if err != nil {
        // try to get the exit code
        if exitError, ok := err.(*exec.ExitError); ok {
            ws := exitError.Sys().(syscall.WaitStatus)
            exitCode = ws.ExitStatus()
        } else {
            // This will happen (in OSX) if `name` is not available in $PATH,
            // in this situation, exit code could not be get, and stderr will be
            // empty string very likely, so we use the default fail code, and format err
            // to string and set to stderr
            log.Printf("Could not get exit code for failed program: %v, %v", name, args)
            exitCode = defaultFailedCode
            if stderr == "" {
                stderr = err.Error()
            }
        }
    } else {
        // success, exitCode should be 0 if go is ok
        ws := cmd.ProcessState.Sys().(syscall.WaitStatus)
        exitCode = ws.ExitStatus()
    }
    // log.Printf("command result, stdout: %v, stderr: %v, exitCode: %v", stdout, stderr, exitCode)
    return
}

func getSysFacts(ifname string)(hn string, ipAddr string) {
	hn, err := os.Hostname()
	if err != nil {
		log.Println("Unable to get hostname")
	}

	interfaces, _ := net.Interfaces()
	for _, inter := range interfaces {
		if inter.Name == ifname {
			if addrs, err := inter.Addrs(); err == nil {
				for _, addr := range addrs {
					switch ip := addr.(type) {
					case *net.IPNet:
						if ip.IP.DefaultMask() != nil {
							ipAddr = ip.IP.String()
						}
					}
				}
			}
		}
	}
	return
}

func main() {
	var config Config
    source, err := ioutil.ReadFile("servicecheck.yml")
    if err != nil {
        log.Fatal(err)
    }
    err = yaml.Unmarshal(source, &config)
    if err != nil {
        panic(err)
    }

	lf, err := os.OpenFile(config.Logfile, os.O_RDWR | os.O_CREATE | os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	defer lf.Close()
	log.SetOutput(lf)

    for _,service := range config.Services {
		sc := service
		_, _, ec := runCommand("service", sc, "status")
		hn, ip := getSysFacts(config.Ifname)
		ecs := strconv.Itoa(ec)
		if ec != 0 {
			ma := []string{hn, ":: ", ip, " Service ", sc, " has a status exit code of ", ecs, " we will attempt to restart the service"}
			m := strings.Join(ma, "")
			log.Println(m)
			go sendMessageToTelegram(m, config.Telegram.ChatId, config.Telegram.BotId)
			_, _, ec := runCommand("service", sc, "start")
			if ec != 0 {
				ecs := strconv.Itoa(ec)
				ma := []string{hn, ":: ", ip, " Service ", sc, " has a status exit code of ", ecs, " the restart attempt failed please look into this"}
				m := strings.Join(ma, "")
				log.Println(m)
				sendMessageToTelegram(m, config.Telegram.ChatId, config.Telegram.BotId)
			}
		}
		if ec == 0 {
			log.Printf("Service %s has a status exit code of %d, Nothing to see here\n", sc, ec)
		}
    }
}